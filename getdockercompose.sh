unameOut="$(uname -s)"
case "${unameOut}" in
    Darwin*)   
        docker_compose=docker-compose-for-mac.yml
        ;;
    Linux*)     
        docker_compose=docker-compose.yml
        ;;
    *)          
        echo "Sorry not supported $unameOut"
        exit 1
        ;;
esac
echo ${docker_compose}