#to be run from ubuntu host
apt -y install sendmail mailutils
#insert an entry in /etc/hosts like "127.0.0.1 ${hostname} ${hostname}.localdomain"
hostip=$(hostname -i | awk '{print $1}')
#note for digital ocean droplet. change in this file will not persist.
echo "$hostip     $(hostname) $(hostname).localdomain" >> /etc/hosts

./image.sh

compose=$(sh getdockercompose.sh) 2>/dev/null
if [ $? -ne 0 ]
then
    echo "failed with $compose"
    exit $?;
fi

docker-compose --file $compose up -d