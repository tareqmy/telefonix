#!/bin/bash
echo "Setting up liquibase"
: ${DB_USER?"DB_USER not set"}
: ${DB_PASS?"DB_PASS not set"}

cat <<CONF > /liquibase.properties
  driver: org.postgresql.Driver
  classpath:/usr/local/bin/postgresql-42.2.6.jar
  url: jdbc:postgresql://$DATABASE:$DB_PORT/$DB_NAME
  username: $DB_USER
  password: $DB_PASS
CONF
