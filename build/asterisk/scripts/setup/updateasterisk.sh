rm -f asterisk-16-*tar.gz
wget http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-16-current.tar.gz

tar xzvf asterisk-16-current.tar.gz

echo "copying samples"
cp asterisk-16.6.0/configs/basic-pbx/* ../../../../confs/basic-pbx/
cp asterisk-16.6.0/configs/samples/* ../../../../confs/samples/

#in app_voicemai.c change the last index query
#	snprintf(sql, sizeof(sql), "SELECT msgnum FROM %s WHERE dir=? order by msgnum::int desc", odbc_table);

echo "delete source"
rm -rf asterisk-16.6.0
