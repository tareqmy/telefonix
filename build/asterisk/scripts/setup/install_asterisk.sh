#!/bin/sh

ASTERISK_TAR=asterisk-16-current.tar.gz
ASTERISK_SRC=/usr/local/src/asterisk

#Install Asterisk
mkdir $ASTERISK_SRC
cd $ASTERISK_SRC
mv /opt/setup/$ASTERISK_TAR $ASTERISK_SRC/
tar -xzvf $ASTERISK_TAR --strip-components=1
rm -f $ASTERISK_TAR

# configure make and install
# Asterisk requires libjansson >= 2.11 and no system copy was found.
./configure --libdir=/usr/lib64 --with-jansson-bundled
make menuselect.makeopts
# enable odbc storage for voicemail
menuselect/menuselect --enable ODBC_STORAGE menuselect.makeopts
# disable unused/deprecated channel drivers
menuselect/menuselect --disable CHAN_IAX2 menuselect.makeopts
menuselect/menuselect --disable CHAN_SIP menuselect.makeopts
menuselect/menuselect --disable CHAN_MGCP menuselect.makeopts
menuselect/menuselect --disable CHAN_SKINNY menuselect.makeopts
menuselect/menuselect --disable CHAN_UNISTIM menuselect.makeopts
menuselect/menuselect --disable CHAN_OSS menuselect.makeopts
menuselect/menuselect --disable CHAN_PHONE menuselect.makeopts

menuselect/menuselect --disable CDR_CSV menuselect.makeopts
menuselect/menuselect --disable CDR_CUSTOM menuselect.makeopts
menuselect/menuselect --disable CEL_CUSTOM menuselect.makeopts
menuselect/menuselect --disable CDR_SQLITE3_CUSTOM menuselect.makeopts
menuselect/menuselect --disable CEL_SQLITE3_CUSTOM menuselect.makeopts

menuselect/menuselect --disable PBX_AEL menuselect.makeopts
menuselect/menuselect --disable PBX_DUNDI menuselect.makeopts

# disable deprecated resources
menuselect/menuselect --disable RES_MONITOR menuselect.makeopts
menuselect/menuselect --disable RES_ADSI menuselect.makeopts

make
make install
make config
make basic-pbx
make clean dist-clean

#Clean up
rm -rf $ASTERISK_SRC
#clean pjproject downloaded by asterisk
rm -f /tmp/pjproject*
#cleanup asterisk configs
rm -rf /etc/asterisk/*

rm -f /etc/localtime
ln -s /usr/share/zoneinfo/UTC /etc/localtime