yum -y update

yum -y install epel-release kernel-devel make gcc gcc-c++

#the default internal database is sqlite3
yum -y install sqlite-devel

# Asterisk requires libjansson >= 2.11 and no system copy was found.
# yum -y install jansson-devel

#to support --with-pjproject-bundled in asterisk we need additionally
yum -y install wget bzip2 patch libuuid-devel

#for default menuselect
yum -y install ncurses-devel libxml2-devel

#for odbc connection
yum -y install unixODBC-devel libtool-ltdl-devel

#for odbc postgresql
yum -y install postgresql-odbc
# odbcinst -q -d >> /root/buildlog
# echo "select 1" | isql -v asterisk-connector asterisk asterisk >> /root/buildlog

#utilities. note postgresql is for the client it is not the server. dont worry.
yum -y install vim unzip ssmtp postgresql-devel

#for ./configure 'libedit-devel'
yum -y install libedit-devel

#shows an error in configure?
yum -y install file

#without it, chan_pjsip fails to load because of undefined symbol ast_sip_cli_traverse_objects
yum -y install openssl-devel

yum autoremove -y
yum clean metadata
yum clean all
rm -rf /var/cache/yum