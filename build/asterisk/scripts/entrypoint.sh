#!/bin/bash

#handle and rotate asterisk log files
/usr/sbin/logrotate -d /etc/logrotate.conf 2> /tmp/logrotate.debug

#change timezone
rm -f /etc/localtime
ln -s /usr/share/zoneinfo/UTC /etc/localtime

# /etc/odbc.ini
if [ -n "$DATABASE" ]; then
  echo "setting $DATABASE in odbc.ini"
  sed "s/Servername = .*/Servername = $DATABASE/" -i /etc/odbc.ini
fi

if [ -n "$DB_PORT" ]; then
  echo "setting $DB_PORT in odbc.ini"
  sed "s/Port = .*/Port = $DB_PORT/" -i /etc/odbc.ini
fi

if [ -n "$DB_NAME" ]; then
  echo "setting $DB_NAME in odbc.ini"
  sed "s/Database = .*/Database = $DB_NAME/" -i /etc/odbc.ini
fi

# /etc/asterisk/res_odbc.conf
if [ -n "$DB_USER" ]; then
  echo "setting user in res_odbc"
  sed "s/username => .*/username => $DB_USER/" -i /etc/asterisk/res_odbc.conf
fi

if [ -n "$DB_PASS" ]; then
  echo "setting password in res_odbc"
  sed "s/password => .*/password => $DB_PASS/" -i /etc/asterisk/res_odbc.conf
fi

# /etc/asterisk/manager.conf
if [ -n "$AMI_USER" ]; then
  echo "setting user in manager"
  sed "s/\[asterisk\]/[$AMI_USER]/" -i /etc/asterisk/manager.conf
fi

if [ -n "$AMI_PASS" ]; then
  echo "setting password in manager"
  sed "s/secret = .*/secret = $AMI_PASS/" -i /etc/asterisk/manager.conf
fi

# /etc/asterisk/ari.conf
if [ -n "$ARI_USER" ]; then
  echo "setting user in ari"
  sed "s/\[getafix\]/[$ARI_USER]/" -i /etc/asterisk/ari.conf
fi

if [ -n "$ARI_PASS" ]; then
  echo "setting password in ari"
  sed "s/password = .*/password = $ARI_PASS/" -i /etc/asterisk/ari.conf
fi

# /etc/asterisk/pjsip.conf
if [ -n "$HOST_IP" ]; then
  echo "setting external ip $HOST_IP in pjsip.conf"
  sed "s/.*external_media_address=.*/external_media_address=$HOST_IP/" -i /etc/asterisk/pjsip.conf
  sed "s/.*external_signaling_address=.*/external_signaling_address=$HOST_IP/" -i /etc/asterisk/pjsip.conf
else
  echo "removing external ip $HOST_IP in pjsip.conf"
  sed "s/.*external_media_address=.*/;external_media_address=$HOST_IP/" -i /etc/asterisk/pjsip.conf
  sed "s/.*external_signaling_address=.*/;external_signaling_address=$HOST_IP/" -i /etc/asterisk/pjsip.conf
fi

# /etc/asterisk/rtp.conf
if [ -n "$RTP_START" ]; then
  echo "setting external ip $RTP_START in rtp.conf"
  sed "s/.*rtpstart=.*/rtpstart=$RTP_START/" -i /etc/asterisk/rtp.conf
else
  echo "removing external ip $RTP_START in rtp.conf"
  sed "s/.*rtpstart=.*/;rtpstart=$RTP_START/" -i /etc/asterisk/rtp.conf
fi

if [ -n "$RTP_END" ]; then
  echo "setting external ip $RTP_END in rtp.conf"
  sed "s/.*rtpend=.*/rtpend=$RTP_END/" -i /etc/asterisk/rtp.conf
else
  echo "removing external ip $RTP_END in rtp.conf"
  sed "s/.*rtpend=.*/;rtpend=$RTP_END/" -i /etc/asterisk/rtp.conf
fi

# /etc/ssmtp/ssmtp.conf
if [ -n "$MAILSERVER" ]; then
  echo "setting mailhub in ssmtp"
  sed "s/mailhub=.*/mailhub=$MAILSERVER/" -i /etc/ssmtp/ssmtp.conf
fi
# /etc/ssmtp/revaliases
if [ -n "$MAILSERVER" ]; then
  echo "setting mailhub in revaliases"
  sed "s/root:.*/root:asterix@telefonix.com:$MAILSERVER/" -i /etc/ssmtp/revaliases
fi

count=0
while [ ! -f /status/done ]
do
  sleep 1
  count=$((count + 1))
  echo "count: $count waiting....$(date)"
  if [ "$count" -eq 500 ]; then
    exit 1;
  fi
done

echo "Found it!"

echo "check for database connection..."
/opt/wait-for-it.sh
echo "database connection checked.."

## Start Asterisk
/usr/sbin/asterisk -vvvvvf