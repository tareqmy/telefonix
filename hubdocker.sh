source .env

# ./delete.sh

./image.sh

docker tag $LIQUIBASE:latest tareqmy/$LIQUIBASE:latest
docker tag $ASTERISK:latest tareqmy/$ASTERISK:latest
docker push tareqmy/$LIQUIBASE:latest
docker push tareqmy/$ASTERISK:latest
docker rmi tareqmy/$LIQUIBASE:latest
docker rmi tareqmy/$ASTERISK:latest