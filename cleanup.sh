echo "Cleaning up before leaving..."
rm -rf confs/
rm -rf changelogs/
rm -rf build/
rm -f Dockerfile docker-compose-rancher.yml docker-compose-for-mac.yml docker-compose.yml
rm -f install.sh image.sh push.sh rebuild.sh update_rancher_confs.sh db.sh shell.sh deploy.sh getdockercompose.sh
rm -f notes
rm -rf .vscode/ .env
echo "Fin."