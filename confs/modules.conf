[modules]
autoload = no

; Realtime support
; Now, we need to configure Asterisk to load its ODBC driver at an early stage of startup, 
; so that it's available when any other modules might need to take advantage of it.
preload => res_odbc.so
preload => res_config_odbc.so

; This is a minimal module load. We are loading only the modules required for
; the Asterisk features used in the Super Awesome Company configuration.

; Applications

load = app_bridgewait.so
load = app_dial.so
load = app_playback.so
load = app_stack.so
load = app_verbose.so
load = app_voicemail.so
load = app_directory.so
load = app_confbridge.so
load = app_queue.so
load = app_while.so
load = app_read.so
load = app_stasis.so
;load = app_cdr.so
;load = app_channelredirect.so
;load = app_chanspy.so
;load = app_controlplayback.so
;load = app_db.so
;load = app_directed_pickup.so
;load = app_dumpchan.so
;load = app_exec.so
;load = app_macro.so
;load = app_mixmonitor.so
;load = app_originate.so
;load = app_page.so
;load = app_playtones.so
;load = app_readexten.so
;load = app_senddtmf.so
;load = app_sendtext.so
;load = app_system.so
;load = app_talkdetect.so
;load = app_transfer.so
;load = app_waituntil.so


; Bridging

load = bridge_builtin_features.so
load = bridge_builtin_interval_features.so
load = bridge_holding.so
load = bridge_native_rtp.so
load = bridge_simple.so
load = bridge_softmix.so

; Call Detail Records

load = cdr_adaptive_odbc.so

; Channel Event Logging

load = cel_manager.so

; Channel Drivers

load = chan_bridge_media.so
load = chan_pjsip.so

; Codecs

load = codec_gsm.so
load = codec_resample.so
load = codec_ulaw.so
load = codec_g722.so

; Formats

load = format_gsm.so
load = format_pcm.so
load = format_wav_gsm.so
load = format_wav.so

; Functions

load = func_callerid.so
load = func_channel.so
load = func_cdr.so
load = func_pjsip_aor.so
load = func_pjsip_contact.so
load = func_pjsip_endpoint.so
load = func_sorcery.so
load = func_devstate.so
load = func_strings.so
load = func_logic.so
;load = func_odbc.so
;load = func_aes.so
;load = func_audiohookinherit.so
;load = func_base64.so
;load = func_blacklist.so
;load = func_callcompletion.so
;load = func_config.so
;load = func_cut.so
;load = func_db.so
;load = func_dialgroup.so
;load = func_dialplan.so
;load = func_enum.so
;load = func_env.so
;load = func_extstate.so
;load = func_frame_trace.so
;load = func_global.so
;load = func_groupcount.so
;load = func_hangupcause.so
;load = func_holdintercept.so
;load = func_iconv.so
;load = func_jitterbuffer.so
;load = func_lock.so
;load = func_math.so
;load = func_md5.so
;load = func_module.so
;load = func_periodic_hook.so
;load = func_pitchshift.so
;load = func_presencestate.so
;load = func_rand.so
;load = func_realtime.so
;load = func_sha1.so
;load = func_shell.so
;load = func_sprintf.so
;load = func_srv.so
;load = func_sysinfo.so
;load = func_talkdetect.so
;load = func_timeout.so
;load = func_uri.so
;load = func_version.so
;load = func_vmcount.so
;load = func_volume.so

; Core/PBX

load = pbx_config.so

; Resources

;load = res_odbc.so
;load = res_odbc_transaction.so
load = res_convert.so
load = res_musiconhold.so
load = res_pjproject.so
load = res_pjsip_acl.so
load = res_pjsip_authenticator_digest.so
load = res_pjsip_caller_id.so
load = res_pjsip_dialog_info_body_generator.so
load = res_pjsip_diversion.so
load = res_pjsip_dtmf_info.so
load = res_pjsip_endpoint_identifier_anonymous.so
load = res_pjsip_endpoint_identifier_ip.so
load = res_pjsip_endpoint_identifier_user.so
load = res_pjsip_exten_state.so
load = res_pjsip_header_funcs.so
load = res_pjsip_logger.so
load = res_pjsip_messaging.so
load = res_pjsip_mwi_body_generator.so
load = res_pjsip_mwi.so
load = res_pjsip_nat.so
load = res_pjsip_notify.so
load = res_pjsip_one_touch_record_info.so
load = res_pjsip_outbound_authenticator_digest.so
load = res_pjsip_outbound_publish.so
load = res_pjsip_outbound_registration.so
load = res_pjsip_path.so
load = res_pjsip_pidf_body_generator.so
load = res_pjsip_pidf_digium_body_supplement.so
load = res_pjsip_pidf_eyebeam_body_supplement.so
load = res_pjsip_publish_asterisk.so
load = res_pjsip_pubsub.so
load = res_pjsip_refer.so
load = res_pjsip_registrar.so
load = res_pjsip_rfc3326.so
load = res_pjsip_sdp_rtp.so
load = res_pjsip_send_to_voicemail.so
load = res_pjsip_session.so
load = res_pjsip.so
load = res_pjsip_t38.so
;load = res_pjsip_transport_websocket.so
load = res_pjsip_xpidf_body_generator.so
load = res_rtp_asterisk.so
load = res_sorcery_astdb.so
load = res_sorcery_config.so
load = res_sorcery_memory.so
load = res_sorcery_realtime.so
load = res_timing_timerfd.so

load = res_http_websocket.so

load = res_stasis.so
load = res_stasis_recording.so
load = res_stasis_playback.so
load = res_stasis_answer.so
load = res_stasis_snoop.so
load = res_stasis_device_state.so
load = res_ari.so
load = res_ari_applications.so
load = res_ari_asterisk.so
load = res_ari_bridges.so
load = res_ari_channels.so
load = res_ari_device_states.so
load = res_ari_endpoints.so
load = res_ari_events.so
load = res_ari_model.so
load = res_ari_playbacks.so
load = res_ari_recordings.so

; Don't load res_hep.so and kin unless you are using hep monitoring in your network

noload = res_hep.so
noload = res_hep_pjsip.so
noload = res_hep_rtcp.so
