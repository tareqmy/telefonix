source .env

echo "Prepare $LIQUIBASE image build..."
rm -rf build/liquibase/changelogs/
cp -a changelogs build/liquibase/
echo "Rebuilding $LIQUIBASE image..."
docker build -t $LIQUIBASE:latest ./build/liquibase/
imageid=$(docker images $LIQUIBASE | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
echo $imageid
docker rmi $imageid
rm -rf build/liquibase/changelogs/

echo "Prepare $ASTERISK image build..."
rm -rf build/asterisk/confs/
mkdir build/asterisk/confs
cp confs/*.conf build/asterisk/confs/
echo "Rebulding $ASTERISK image..."
docker build -t $ASTERISK:latest ./build/asterisk/
imageid=$(docker images $ASTERISK | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
echo $imageid
docker rmi $imageid
rm -rf build/asterisk/confs/

imageid=$(docker images | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid
