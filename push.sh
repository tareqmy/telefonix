source .env

# ./delete.sh

./image.sh

docker tag $LIQUIBASE:latest tareqmy.vantageip.com:5000/$LIQUIBASE:latest
docker tag $ASTERISK:latest tareqmy.vantageip.com:5000/$ASTERISK:latest
docker push tareqmy.vantageip.com:5000/$LIQUIBASE:latest
docker push tareqmy.vantageip.com:5000/$ASTERISK:latest
docker rmi tareqmy.vantageip.com:5000/$LIQUIBASE:latest
docker rmi tareqmy.vantageip.com:5000/$ASTERISK:latest
